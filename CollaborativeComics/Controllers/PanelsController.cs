﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CollaborativeComics.Data;
using CollaborativeComics.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace CollaborativeComics.Controllers
{
    [Authorize]
    public class PanelsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PanelsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Panels
        private async Task<IActionResult> Index()
        {
            return View(await _context.Panel.ToListAsync());
        }

        // GET: Panels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var panel = await _context.Panel
                .Include(p=>p.Comics)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (panel == null)
            {
                return NotFound();
            }

            return View(panel);
        }

        // GET: Panels/Create
        public IActionResult Create(int? id)
        {
            ViewData["comicsId"] = id;
            return View();
        }

        // POST: Panels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int? comicsId, IFormFile image, string description)
        {
            var panel = new Panel();

            panel.Comics = _context.Comics.FirstOrDefault(c => c.Id == comicsId);
            panel.Description = description;
            panel.LastUpdate = DateTime.Now;
            if (image != null)
            {
                var guid = Guid.NewGuid().ToString();
                var extension = Path.GetExtension(image.FileName);
                panel.ImageURI = $"/panels/{guid}{extension}";
                var path = $"{Directory.GetCurrentDirectory()}\\wwwroot\\panels\\{guid}{extension}";
                using (var fs = new FileStream(path, FileMode.Create))
                {
                    await image.CopyToAsync(fs);
                }
            }
            else
            {
                panel.ImageURI = "/panels/default.jpg";
            }

            if (ModelState.IsValid)
            {
                _context.Panel.Add(panel);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Comics", new{id=panel.Comics?.Id});
                //return RedirectToAction(nameof(Index));
            }
            return View("Create");
        }

        // GET: Panels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var panel = await _context.Panel.Include(p=>p.Comics).SingleOrDefaultAsync(m => m.Id == id);
            if (panel == null)
            {
                return NotFound();
            }
            return View(panel);
        }

        // POST: Panels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, IFormFile image, [Bind("Id,LastUpdate,ImageURI,Description")] Panel panel)
        {
            if (id != panel.Id)
            {
                return NotFound();
            }

            panel.LastUpdate = DateTime.Now;

            if (image != null)
            {
                var guid = Guid.NewGuid().ToString();
                var extension = Path.GetExtension(image.FileName);
                panel.ImageURI = $"/panels/{guid}{extension}";
                var path = $"{Directory.GetCurrentDirectory()}\\wwwroot\\panels\\{guid}{extension}";
                using (var fs = new FileStream(path, FileMode.Create))
                {
                    await image.CopyToAsync(fs);
                }
            }
            else
            {
                panel.ImageURI = "/panels/default.jpg";
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(panel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PanelExists(panel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(panel);
        }

        // GET: Panels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var panel = await _context.Panel
                .Include(p=>p.Comics)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (panel == null)
            {
                return NotFound();
            }

            return View(panel);
        }

        // POST: Panels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var panel = await _context.Panel.Include(p=>p.Comics).SingleOrDefaultAsync(m => m.Id == id);
            _context.Panel.Remove(panel);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Comics", new {id = panel.Comics?.Id});
        }

        private bool PanelExists(int id)
        {
            return _context.Panel.Any(e => e.Id == id);
        }
    }
}
