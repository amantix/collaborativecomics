﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CollaborativeComics.Data;
using Microsoft.AspNetCore.Mvc;
using CollaborativeComics.Models;

namespace CollaborativeComics.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var comicsCount = _context.Comics.Count();
            var storiesCount = _context.Story.Count();
            var panelsCount = _context.Panel.Count();
            var usersCount = _context.Users.Count();

            ViewData["comicsCount"] = comicsCount;
            ViewData["storiesCount"] = storiesCount;
            ViewData["panelsCount"] = panelsCount;
            ViewData["usersCount"] = usersCount;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
