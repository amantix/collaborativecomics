﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CollaborativeComics.Data;
using CollaborativeComics.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace CollaborativeComics.Controllers
{
    [Authorize]
    public class ComicsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ComicsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Comics
        public async Task<IActionResult> Index()
        {
            return View(await _context.Comics.ToListAsync());
        }

        // GET: Comics/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comics = await _context.Comics
                .Include(c=>c.Panels)
                .Include(c=>c.Story)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (comics == null)
            {
                return NotFound();
            }

            return View(comics);
        }

        // GET: Comics/Create
        public IActionResult Create(int? id)
        {
            if(id!=null)
                ViewData.Add("storyId", (int)id);
            return View();
        }

        // POST: Comics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int? storyId, string Name, IFormFile cover)
        {
            var comics = new Comics();
            if(storyId!=null)
                comics.Story = _context.Story.First(s => s.Id == storyId);
            comics.Name = Name;

            if (cover != null)
            {
                var guid = Guid.NewGuid().ToString();
                var extension = Path.GetExtension(cover.FileName);
                comics.CoverURI = $"/covers/{guid}{extension}";

                var path = $"{Directory.GetCurrentDirectory()}\\wwwroot\\covers\\{guid}{extension}";
                using (var fs = new FileStream(path, FileMode.Create))
                {
                    await cover.CopyToAsync(fs);
                }
            }
            else comics.CoverURI = "/covers/default.jpg";

            if (ModelState.IsValid)
            {
                _context.Add(comics);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(comics);
        }

        // GET: Comics/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comics = await _context.Comics.SingleOrDefaultAsync(m => m.Id == id);
            if (comics == null)
            {
                return NotFound();
            }
            return View(comics);
        }

        // POST: Comics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, IFormFile cover, [Bind("Id,Name,CoverURI")] Comics comics)
        {
            if (id != comics.Id)
            {
                return NotFound();
            }

            if (cover != null)
            {
                var guid = Guid.NewGuid().ToString();
                var extension = Path.GetExtension(cover.FileName);
                comics.CoverURI = $"/covers/{guid}{extension}";
                var path = $"{Directory.GetCurrentDirectory()}\\wwwroot\\covers\\{guid}{extension}";
                using (var fs = new FileStream(path, FileMode.Create))
                {
                    await cover.CopyToAsync(fs);
                }
            }
            else
            {
                comics.CoverURI = "/covers/default.jpg";
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(comics);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ComicsExists(comics.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(comics);
        }

        // GET: Comics/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comics = await _context.Comics
                .SingleOrDefaultAsync(m => m.Id == id);
            if (comics == null)
            {
                return NotFound();
            }

            return View(comics);
        }

        // POST: Comics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var comics = await _context.Comics.SingleOrDefaultAsync(m => m.Id == id);
            _context.Comics.Remove(comics);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ComicsExists(int id)
        {
            return _context.Comics.Any(e => e.Id == id);
        }
    }
}
