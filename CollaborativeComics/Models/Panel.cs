﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CollaborativeComics.Models
{
    public class Panel
    {
        public int Id { get; set; }

        [Display(Name = "Авторы")]
        public List<ApplicationUser> Contributors { get; set; } = new List<ApplicationUser>();

        [Display(Name = "Последнее изменение")]
        public DateTime LastUpdate { get; set; }

        public string ImageURI { get; set; }

        public Comics Comics { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }
    }
}
