﻿using System.Collections.Generic;

namespace CollaborativeComics.Models
{
    public class Comics
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CoverURI { get; set; }

        public Story Story { get; set; }

        public List<Panel> Panels { get; set; } = new List<Panel>();

    }
}
