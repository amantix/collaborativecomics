﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CollaborativeComics.Models
{
    public class Story
    {
        public int Id { get; set; }

        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Автор")]
        public ApplicationUser Author { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Время создания")]
        public DateTime CreationTime { get; set; } = DateTime.Now;

        [Display(Name = "Описание")]
        public string Description { get; set; }

        public List<Comics> Comics { get; set; }
    }
}
